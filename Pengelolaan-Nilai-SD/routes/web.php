<?php

use App\Http\Controllers\Guru_Kelas_MapelController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\MapelController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\NilaiStoreController;
use App\Http\Controllers\SiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});
Route::get('/login', function(){
    return view('layout.login');
});
Route::get('/dashboard', function(){
    return view('dashboard.index');
});

Route::controller(SekolahController::class)->group(function () {
    Route::get('/sekolah', 'index');
    Route::get('/sekolah/{npsn}/edit', 'edit'); 
    Route::put('/sekolah/{npsn}', 'update');
});
// detail = ngajar
Route::resource('guru', GuruController::class);
Route::resource('mapel', MapelController::class)->except('show');
Route::resource('kelas', KelasController::class);
Route::resource('siswa', SiswaController::class);
Route::resource('kelas.ngajar', Guru_Kelas_MapelController::class)->shallow();
Route::controller(NilaiController::class)->group(function () {
    Route::get('/nilai', 'index');
    Route::get('/nilai/{id}', 'mapel');
    Route::get('/nilai/{kelas}/{mapel}/create', 'input');
    Route::get('/nilai/{tes}/edit', 'input');
    Route::put('/nilai/{kelas}/{mapel}', 'update'); 
});



// Route::resource('nilai.input', NilaiStoreController::class)->except('index');
