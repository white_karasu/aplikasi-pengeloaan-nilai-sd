@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Edit Mata</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/kelas/{{$kelas->id_kelas}}/ngajar" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/ngajar/{{$ngajar->id_ngajar}}" method="POST">
        @csrf
        @method('put')
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <input type="hidden" value="{{$ngajar->kelas_id}}" name="kelas_id">
            <tr>
              <th><h5>Nama Mata Pelajaran</h5></th>
              <th><h5>Guru Pengampu</h5></th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td><h6>
                    <select name="mapel_id" id="">
                    @foreach ($mapel as $item)
                    @if ($item->id_mapel === $ngajar->mapel_id)
                    <option value="{{$item->id_mapel}}" selected>{{$item->namaMapel}}</option>    
                    @else
                    <option value="{{$item->id_mapel}}">{{$item->namaMapel}}</option>   
                    @endif
                    @endforeach</h6></select></td>
                <td>
                    <select name="guru_id" id="">
                    <option value="">--Pilih Guru Pengampu--</option>
                    @foreach ($guru as $value)
                    @if ($value->nipGuru === $ngajar->guru_id)
                    <option name="guru_id" value="{{$value->nipGuru}}" selected>{{$value->namaGuru}}</option>    
                    @else
                    <option name="guru_id" value="{{$value->nipGuru}}">{{$value->namaGuru}}</option>    
                    @endif
                    @endforeach
                    </select>
                </td>
              </tr>  
              </tbody>
            </table>
            <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection