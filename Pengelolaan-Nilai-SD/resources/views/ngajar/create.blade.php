@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Tambah Mata Pelajaran ke {{$kelas->namaKelas}}</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/kelas/{{$kelas->id_kelas}}/ngajar" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/kelas/{{$kelas->id_kelas}}/ngajar" method="POST">
        @csrf
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <input type="hidden" value="{{$kelas->id_kelas}}" name="kelas_id">
            <tr>
              <th> # </th>
              <th><h5>Nama Mata Pelajaran</h5></th>
              <th><h5>Guru Pengampu</h5></th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($data as $key => $item)
            <tr >
              <td>{{$key + 1}}</td>
              <td><h6>{{$item->namaMapel}}</h6></td>  
               <td>
                <select name="guru_id[]" id="">
                <option value="">--Pilih Guru Pengampu--</option>
                @foreach ($guru as $value)
                    <option name="guru_id[]" value="{{$value->nipGuru}}">{{$value->namaGuru}}</option>
                @endforeach
                </select>
            </td>
            <td><input type="checkbox" name="mapel_id[]" id="mapel_id[]" value="{{$item->id_mapel}}"></td>  
            </tr>    
            
            @empty
                <h4>Data Kosong</h4>
                                
            @endforelse
            
              </tbody>
            </table>
      <button type="submit" class="btn btn-info float-right">Selesai</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection