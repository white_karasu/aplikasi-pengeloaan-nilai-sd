@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
     <div class="page_title">
        <h1>Menginput Nilai</h1>
     </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          @foreach ($kelas as $item)
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">{{$item->namaKelas}}</h5>
                <a href="/nilai/{{$item->id_kelas}}" class="btn btn-primary">Pilih</a>
              </div>
            </div>
          </div>    
          @endforeach
          </div>
      </div>
    </div>
  </div>
@endsection