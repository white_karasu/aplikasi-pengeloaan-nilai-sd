@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
     <div class="page_title">
        <h1>{{$kelas->namaKelas}} / </h1>
     </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          @foreach ($mapel as $item)
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">{{$item->mapel->namaMapel}}</h5>
                <a href="/nilai/{{$kelas->id_kelas}}/{{$item->id_ngajar}}/create" class="btn btn-primary">Pilih</a>
              </div>
            </div>
            <br>
          </div>
          <br>    
          @endforeach
          </div>
          <br>
      </div>
    </div>
  </div>
@endsection
