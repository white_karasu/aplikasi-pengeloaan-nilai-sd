@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h4>{{$kelas->namaKelas}} / </h4>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/nilai/{{$kelas->id_kelas}}/" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="" method="POST">
        @csrf
        @method('put')
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><h5>No Absen</h5></th>
              <th> <h5>Nama Siswa</h5></th>
              <th> <h5>Tugas Sekolah 1</h5></th>
              <th> <h5>Tugas Sekolah 2</h5></th>
              <th> <h4>UTS</h4></th>
              <th> <h5>Tugas Sekolah 3</h5></th>
              <th> <h5>Tugas Sekolah 4</h5></th>
              <th><h4>UAS</h4></th>
              <th><h5>Nilai Akhir</h5></th>
            </tr>
          </thead>
          <tbody>
            @forelse ($siswa as $key => $item)
            <tr align="center">
              <td><h6>{{$key + 1}}</h6></td>
              {{-- <input type="number" hidden name="kelas_id" value="{{$kelas->id_kelas}}">--}}
              <input type="number" hidden name="mapel_id" value="{{$ngajar->mapel_id}}">
              <td><h6><input type="number" hidden name="siswa_id[]" value="{{$item->siswa->namaSiswa}}">{{$item->siswa->namaSiswa}}</h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="ts1[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="ts2[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="uts[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="ts3[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="ts4[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="uas[]"></h6></td>
              <td><h6><input type="number" style="width: 60px;height: 40px" name="na[]"></h6></td>
            </tr>                                        
          </tbody>                   
          @empty
          <h4>Data Kosong</h4>   
         @endforelse  
      </table>
      <button type="submit" class="btn btn-info float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection