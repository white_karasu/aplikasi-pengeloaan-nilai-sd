@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Kelas 1 / Kemanusiaan</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> # </th>
              <th>
                <h5>Nama Murid</h5>
              </th>
              <th>
                <h5>Nilai</h5>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text" value="80"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
            <tr>
              <td>~</td>
              <td>
                <h6>Ijat</h6>
              </td>
              <td><input type="text"></td>
            </tr>
          </tbody>
        </table>
        <h4><a href="mapel-tambah" class="btn btn-primary my-3">Selesai</a> </h4>
        <script>
          $(document).ready(function() {
            $('#tabel').DataTable();
          });
        </script>
      </div>
    </div>
  </div>
</div>
@endsection