@extends('layouts.master')
@section('user')
<div class="row column_title">
  <div class="col-md-12">
     <div class="page_title">
        <h2>Selamat Datang User</h2>
     </div>
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="card-group"></div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Sekolah</h5>
        <br>
        <a href="/sekolah" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Guru</h5>
        <br>
        <a href="/guru" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Mata Pelajaran</h5>
        <br>
        <a href="#" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Siswa</h5>
        <br>
        <a href="#" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="card-group"></div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Kelas</h5>
        <br>
        <a href="/sekolah" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Nilai</h5>
        <br>
        <a href="/guru" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Kelola Data</h5>
        <h5 class="card-title">Rapot</h5>
        <br>
        <a href="#" class="btn btn-primary">Klik Disini</a>
      </div>
    </div>
  </div>
</div>
@endsection