@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Edit Biodata Guru</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <p><a href="/guru" class="btn btn-primary my-3 mx-4">Kembali</a></p>
    <div class="card-body">
      <form action="/guru/{{$guru->nipGuru}}" method="post">
        @csrf
        @method('put')
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>NIP Guru</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nipGuru" value="{{$guru->nipGuru}}" required> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaGuru" value="{{$guru->namaGuru}}"> </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection