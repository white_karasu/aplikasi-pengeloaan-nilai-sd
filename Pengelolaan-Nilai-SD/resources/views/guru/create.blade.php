@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Tambah Guru</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/guru" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/guru" method="post">
        @csrf
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Nip Guru</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nipGuru" required> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaGuru" required> </td>
          </tr>
          <tr>
            <td>
              <h6>Jenis Kelamin</h6>
            </td>
            <td> : </td>
            <td> 
              <select name="jkGuru" id=""> 
                <option value="">--Pilih Jenis Kelamin--</option>
                <option value="laki-laki">laki - laki</option>
                <option value="perempuan">perempuan</option>
              </select>
            </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-primary float-right">Selesai</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection