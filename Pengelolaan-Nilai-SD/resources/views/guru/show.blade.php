@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Biodata Guru</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>NIP Guru</h6>
            </td>
            <td> : </td>
            <td> <h6>{{$guru->nipGuru}}</h6></td>
          </tr>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> <h6>{{$guru->namaGuru}}</h6> </td>
          </tr>
          <tr>
            <td>
              <h6>Jenis Kelamin</h6>
            </td>
            <td> : </td>
            <td> <h6>{{$guru->jkGuru}}</h6> </td>
          </tr>
        </tbody>
      </table>
      <a href="/guru" class="btn btn-primary my-3 float-left">Kembali</a>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection