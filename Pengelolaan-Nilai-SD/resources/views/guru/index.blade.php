@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><a href="/guru/create" class="btn btn-primary my-3">Tambah Guru</a> </h4>
      </p>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> # </th>
              <th><h5>NIP</h5></th>
              <th><h5>Nama Lengkap</h5></th>
              <th><h5>Jenis Kelamin</h5></th>
              <th><h5></h5></th>
            </tr>
          </thead>
          <tbody>
            @forelse ($guru as $key => $item)
            <tr>
              <td>{{$key + 1}}</td>
              <td><h6>{{$item->nipGuru}}</h6></td>
              <td><h6>{{$item->namaGuru}}</h6></td>
              <td><h6>{{$item->jkGuru}}</h6></td>
              <td>
                <form action="/guru/{{$item->nipGuru}}" method="POST" class="float-right">
                  @csrf
                  @method('delete')
                  <a href="/guru/{{$item->nipGuru}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/guru/{{$item->nipGuru}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Hapus">  
              </form>
              </td>
            </tr> 
            @empty
            <h4>Data Kosong</h4>
        @endforelse 
          </tbody>
        </table>
        <script>
          $(document).ready(function() {
            $('#tabel').DataTable();
          });
        </script>
      </div>
    </div>
  </div>
</div>
@endsection