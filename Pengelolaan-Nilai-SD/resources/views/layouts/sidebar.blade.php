<nav id="sidebar">
   <div class="sidebar_blog_1">
      <div class="sidebar-header">
         <div class="logo_section">
            <a href="index.html"><img class="logo_icon img-responsive" src="{{asset('asset/images/logo/icon.png')}}" alt="#" /></a>
         </div>
      </div>
      <div class="sidebar_user_info">
         <div class="icon_setting"></div>
         <div class="user_profle_side">
            <div class="user_img"><img class="img-responsive" src="{{asset('asset/images/layout_img/takin.jpeg')}}" alt="#" /></div>
            <div class="user_info">
               <h6>John David</h6>
            </div>
         </div>
      </div>
   </div>
   <div class="sidebar_blog_2">
      <h4>Navigasi</h4>
      <ul class="list-unstyled components">
         <li class="active">
            <a href="/dashboard">
               <i class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a>
         </li>
         <li>
            <a href="/sekolah">
               <i class="fa fa-cloud"></i> <span>Data Sekolah</span></a>
         </li>
         <li>
            <a href="/guru">
               <i class="fa fa-user purple_color"></i> <span>Data Guru</span></a>
         </li>
         <li><a href="/mapel">
            <i class="fa fa-briefcase blue1_color"></i> <span>Data Mata Pelajaran</span></a>
         </li>
         <li>
            <a href="/kelas">
            <i class="fa fa-paper-plane red_color"></i> <span>Data Kelas</span></a>
         </li>
         <li><a href="/siswa">
            <i class="fa fa-star blue1_color" aria-hidden="true"></i> <span>Data Siswa</span></a>
         </li>    
         <li>
            <a href="/nilai">
         <i class="fa fa-book"></i> <span>Data Nilai</span></a>
         </li>
         <li>
            <a href="/rapot">
         <i class="fa fa-database"></i> <span>Raport</span></a>
         </li>
      </ul>
   </div>
   <div class="sidebar_blog_2">
      <h4>MTC inc</h4>
</nav>