@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Tambah Mata Pelajaran</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/mapel" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/mapel" method="POST">
        @csrf
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th><h4>Data</h4></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h6>Nama Mata Pelajaran</h6></td>
            <td> <input type="text" name="namaMapel"> </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-info float-right">Selesai</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>


@endsection