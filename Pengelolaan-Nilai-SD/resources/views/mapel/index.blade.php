@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4><a href="/mapel/create" class="btn btn-primary my-3">Tambah Mata Pelajaran</a> </h4>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> # </th>
              <th><h5>Nama Mata Pelajaran</h5></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @forelse ($mapel as $key => $item)
            <tr>
              <td>{{$key + 1}}</td>
              <td><h6>{{$item->namaMapel}}</h6></td>
              <td>
                <form action="/mapel/{{$item->id_mapel}}" method="POST" class="float-right">
                  @csrf
                  @method('delete')
                  <a href="/mapel/{{$item->id_mapel}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Hapus">  
              </form>
              </td>
            </tr>
            @empty
                <h4>Data Kosong</h4>
            @endforelse
              </tbody>
            </table>
            <script>
              $(document).ready(function() {
                $('#tabel').DataTable();
              });
            </script>
          </div>
        </div>
      </div>
    </div>
    @endsection