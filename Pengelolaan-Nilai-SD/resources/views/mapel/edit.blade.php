@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Edit Mata Pelajaran</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <p><a href="/mapel" class="btn btn-primary my-3 mx-4">Kembali</a></p>
    <div class="card-body">
      <form action="/mapel/{{$mapel->id_mapel}}" method="POST">
        @csrf
        @method('PUT')
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th><h4>Data</h4></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h6>Nama Mata Pelajaran</h6></td>
            <td> <input type="text" name="namaMapel" value="{{$mapel->namaMapel}}"> </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>


@endsection