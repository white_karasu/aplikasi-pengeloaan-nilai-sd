@extends('layout.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><a href="guru-tambah" class="btn btn-primary my-3">Tambah Guru</a> </h4>
      </p>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> # </th>
              <th>
                <h5>Nama Lengkap</h5>
              </th>
              <th>
                <h5>NIP</h5>
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>~</td>
              <td>
                <h6>Socrates</h6>
              </td>
              <td>
                <h6>12345</h6>
              </td>
              <td>
                <form action="" method="POST" class="float-right">
                  @csrf
                  @method('delete')
                  <a href="#" class="btn btn-info btn-sm">Detail</a>
                  <a href="/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Hapus">  
              </form>
              </td>
            </tr>
          </tbody>
        </table>
        <script>
          $(document).ready(function() {
            $('#tabel').DataTable();
          });
        </script>
      </div>
    </div>
  </div>
</div>
@endsection