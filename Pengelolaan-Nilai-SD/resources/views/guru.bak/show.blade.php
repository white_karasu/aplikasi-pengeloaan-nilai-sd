@extends('layout.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Biodata Guru</h1>

    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> {$namaGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>NIK</h6>
            </td>
            <td> : </td>
            <td> {$nikGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>NIP</h6>
            </td>
            <td> : </td>
            <td> {$nipGuru}</td>
          </tr>
          <tr>
            <td>
              <h6>NUPTK</h6>
            </td>
            <td> : </td>
            <td> {$nuptkGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Lahir</h6>
            </td>
            <td> : </td>
            <td> {$tmptlhrGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal Lahir</h6>
            </td>
            <td> : </td>
            <td> {$ttlGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Umur</h6>
            </td>
            <td> : </td>
            <td> {$umurGuru} </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Jenis Kelamin</h6>
            </td>
            <td> : </td>
            <td>{$jkGuru}</select>
            </td>
          </tr>
          <tr>
            <td>
              <h6>Agama</h6>
            </td>
            <td> : </td>
            <td> {$agamaGuru} </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Status Perkawinan</h6>
            </td>
            <td> : </td>
            <td> {$statuskwnGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> {$pasanganGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Pekerjaan Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> {$pekerjaanIstriGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Ibu Kandung</h6>
            </td>
            <td> : </td>
            <td> {$namaIbuKandungGuru} </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Pendidikan Terakhir</h6>
            </td>
            <td> : </td>
            <td> {$pendidikanGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Universitas/Sekolah Tinggi</h6>
            </td>
            <td> : </td>
            <td> {$namaPendidikanGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Program Studi/Jurusan</h6>
            </td>
            <td> : </td>
            <td> {$prodiGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Induk Mahasiswa</h6>
            </td>
            <td> : </td>
            <td> {$nimGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Masuk</h6>
            </td>
            <td> : </td>
            <td> {$tahunMasukUnivGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Lulus</h6>
            </td>
            <td> : </td>
            <td> {$tahunLulusUnivGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>IPK</h6>
            </td>
            <td> : </td>
            <td> {$ipkGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Induk Mahasiswa</h6>
            </td>
            <td> : </td>
            <td>{$nimGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>TMT Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> {$pengangkatanGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>NO SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> {$noPengangkatanSkGuru}</td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> {$tglSkPengangkatan} </td>
          </tr>
          <tr>
            <td>
              <h6>TMT PNS</h6>
            </td>
            <td> : </td>
            <td> {$tmtGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>No SK PNS</h6>
            </td>
            <td> : </td>
            <td> {$noSkGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK PNS</h6>
            </td>
            <td> : </td>
            <td> {$tglSkGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>TMT SK Berkala</h6>
            </td>
            <td> : </td>
            <td> {$tmtSkBerkalaGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>No SK Berkala</h6>
            </td>
            <td> : </td>
            <td> {$noSkBerkalaGuru}</td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Berkala</h6>
            </td>
            <td> : </td>
            <td> {$tglSkBerkalaGuru}</td>
          </tr>
          <tr>
            <td>
              <h6>TMT Di Sekolah Ini</h6>
            </td>
            <td> : </td>
            <td> {$tmtDiSekolahIniGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK di Sekolah ini</h6>
            </td>
            <td> : </td>
            <td> {$tglSkDiSekolahIniGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Sertifikat Pendidik</h6>
            </td>
            <td> : </td>
            <td> {$nomorSertifikatGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Peserta Sertifikasi</h6>
            </td>
            <td> : </td>
            <td> {$nomorPesertaSertifikatGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Di SD Ini</h6>
            </td>
            <td> : </td>
            <td> {$masaKerjaGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Seluruhnya</h6>
            </td>
            <td> : </td>
            <td> {$masaKerjaSeluruhnyaGuru} </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Tugas</h6>
            </td>
            <td> : </td>
            <td> {$tmptTugasGuru} </td>
          </tr>
        </tbody>
      </table>
      <h4><a href="mapel-tambah" class="btn btn-primary my-3 float-left">Kembali</a> </h4>

      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection