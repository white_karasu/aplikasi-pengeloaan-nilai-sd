@extends('layout.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Tambah Guru</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="mapel-tambah" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/guru" action="post">
        @csrf
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>NIK</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nikGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>NIP</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nipGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>NUPTK</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nuptkGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Lahir</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="tmptlhrGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal Lahir</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="ttlGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Umur</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="umurGuru"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Jenis Kelamin</h6>
            </td>
            <td> : </td>
            <td> <select name="jkGuru" value="--Jenis Kelamin--">
                <option value="LAKI-LAKI">Laki - Laki</option>
                <option value="PEREMPUAN">Perempuan</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <h6>Agama</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="agamaGuru"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Status Perkawinan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="statuskwnGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pasanganGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Pekerjaan Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pekerjaanIstriGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Ibu Kandung</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaIbuKandungGuru"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Pendidikan Terakhir</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pendidikanGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Universitas/Sekolah Tinggi</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaPendidikanGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Program Studi/Jurusan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="prodiGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Induk Mahasiswa</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nimGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Masuk</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="tahunMasukUnivGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Lulus</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="tahunLulusUnivGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>IPK</h6>
            </td>
            <td> : </td>
            <td> <input type="floatval" name="ipkGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="pengangkatanGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>NO SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="noPengangkatanSkGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkPengangkatan"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>No SK PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="noSkGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtSkBerkalaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>No SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="noSkBerkalaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkBerkalaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT Di Sekolah Ini</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtDiSekolahIniGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK di Sekolah ini</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkDiSekolahIniGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Sertifikat Pendidik</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nomorSertifikatGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Peserta Sertifikasi</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nomorPesertaSertifikatGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Di SD Ini</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="masaKerjaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Seluruhnya</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="masaKerjaSeluruhnyaGuru"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Tugas</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="tmptTugasGuru"> </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-primary float-right">Selesai</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection