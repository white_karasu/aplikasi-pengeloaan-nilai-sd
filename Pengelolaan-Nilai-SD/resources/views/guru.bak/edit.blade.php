@extends('layout.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Edit Biodata Guru</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <form action="" method="post">
        @csrf
        @method('put')
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th> </th>
            <th>
              <h4>Data</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Nama Lengkap</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaGuru" value="{$namaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>NIK</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nikGuru" value="{$nikGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>NIP</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nipGuru" value="{$nipGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>NUPTK</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nuptkGuru" value="{$nuptkGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Lahir</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="tmptlhrGuru" value="{$tmptlhrGuru}"></td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal Lahir</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="ttlGuru" value="{$ttlGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Umur</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="umurGuru" value="{$umurGuru}"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Jenis Kelamin</h6>
            </td>
            <td> : </td>
            <td> <select name="jkGuru" value="--Jenis Kelamin--">
                <option value="LAKI-LAKI">Laki - Laki</option>
                <option value="PEREMPUAN">Perempuan</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <h6>Agama</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="agamaGuru" value="{$agamaGuru}"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Status Perkawinan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="statuskwnGuru" value=" {$statuskwnGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pasanganGuru" value=" {$pasanganGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Pekerjaan Istri/Suami</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pekerjaanIstriGuru" value=" {$pekerjaanIstriGuru} "> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Ibu Kandung</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaIbuKandungGuru" value=" {$namaIbuKandungGuru}"> </td>
          </tr>
          <tr>
          <tr>
            <td>
              <h6>Pendidikan Terakhir</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="pendidikanGuru" value=" {$pendidikanGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Universitas/Sekolah Tinggi</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="namaPendidikanGuru" value="{$namaPendidikanGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Program Studi/Jurusan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="prodiGuru" value="{$prodiGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Induk Mahasiswa</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nimGuru" value="{$nimGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Masuk</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="tahunMasukUnivGuru" value="{$tahunMasukUnivGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Lulus</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="tahunLulusUnivGuru" value="{$tahunLulusUnivGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>IPK</h6>
            </td>
            <td> : </td>
            <td> <input type="floatval" name="ipkGuru" value="{$ipkGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="pengangkatanGuru" value=" {$pengangkatanGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>NO SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="noPengangkatanSkGuru" value="{$noPengangkatanSkGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Pengangkatan</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkPengangkatan" value="{$tglSkPengangkatan}"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtGuru" value="{$tmtGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>No SK PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="noSkGuru" value=" {$noSkGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK PNS</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkGuru" value="{$tglSkGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtSkBerkalaGuru" value="{$tmtSkBerkalaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>No SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="noSkBerkalaGuru" value="{$noSkBerkalaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK Berkala</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkBerkalaGuru" value="{$tglSkBerkalaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>TMT Di Sekolah Ini</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tmtDiSekolahIniGuru" value="{$tmtDiSekolahIniGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tanggal SK di Sekolah ini</h6>
            </td>
            <td> : </td>
            <td> <input type="date" name="tglSkDiSekolahIniGuru" value="{$tglSkDiSekolahIniGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Sertifikat Pendidik</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nomorSertifikatGuru" value="{$nomorSertifikatGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Nomor Peserta Sertifikasi</h6>
            </td>
            <td> : </td>
            <td> <input type="number" name="nomorPesertaSertifikatGuru" value=" {$nomorPesertaSertifikatGuru} "> </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Di SD Ini</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="masaKerjaGuru" value=" {$masaKerjaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Masa Kerja Seluruhnya</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="masaKerjaSeluruhnyaGuru" value="{$masaKerjaSeluruhnyaGuru}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tempat Tugas</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="tmptTugasGuru" value=" {$tmptTugasGuru}"> </td>
          </tr>
        </tbody>
      </table>
      <a href="/guru" class="btn btn-primary my-3 float-left">Kembali</a>
      <button type="submit" class="btn btn-warning float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection