@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
<div class="row column_title">
    <div class="col-md-12">
       <div class="page_title">
          <h1>Data Lengkap {{$kelas->namaKelas}}</h1>
       </div>
    </div>
</div>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">   
          <table class="table table-bordered">
            <thead>
                <tr>
                  <th><h5>No Absen</h5></th>
                  <th> <h5>Nama Siswa</h5></th>
                  <th> <h5>Umur Siswa</h5></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @forelse ($siswa as $key => $item)
                <tr>
                  <td><h6>{{$key + 1}}</h6></td>
                  <td><h6>{{$item->namaSiswa}}</h6></td>
                  <td><h6>{{$item->umurSiswa}}</h6></td>
                  <td>
                      <a href="/siswa/{{$item->id_siswa}}" class="btn btn-info justify-content-center">Detail</a>
                  </td>
                </tr>                                        
              </tbody>                   
              @empty
              <h4>Data Kosong</h4>   
             @endforelse  
          </table>
          <br><br>
        <script>
            $(document).ready(function() {
            $('#tabel').DataTable();
          } );
        </script>
        </div>
        
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-lg-12 grid-margin stretch-card">
    <h4 class="card-title"><a href="/kelas/{{$kelas->id_kelas}}/detail/create" class="btn btn-primary my-3">Tambah Mata Pelajaran ke {{$kelas->namaKelas}}</a> </h4>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">   
          <div class="table-responsive">   
            <table class="table table-bordered">
              <thead>
                  <tr>
                    <th> <h5>Nama Mata Pelajaran</h5></th>
                    <th> <h5>Guru Pengampu</h5></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($ngajar as $key => $item)
                  <tr>
                    <td><h6>{{$item->kelas_id}}</h6></td>
                    <td><h6>{{$item->guru_id}}</h6></td>
                    <td>
                      <form action="/ngajar/{{$item->id_ngajar}}" method="POST" class="float-right">
                        @csrf
                        @method('delete')
                        <a href="/ngajar/{{$item->id_ngajar}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Hapus"> 
                      </form> 
                    </td>
                  </tr>                                        
                </tbody>                   
                @empty
                <h4>Data Kosong</h4>   
               @endforelse  
            </table>
          <br><br>
        <script>
            $(document).ready(function() {
            $('#tabel').DataTable();
          } );
        </script>
        </div>
        
      </div>
    </div>
  </div>
    <h4><a href="/kelas" class="btn btn-primary my-3 float-left">Kembali</a> </h4>
    <br><br>
  <script>
      $(document).ready(function() {
      $('#tabel').DataTable();
    } );
  </script>
  </div>
@endsection