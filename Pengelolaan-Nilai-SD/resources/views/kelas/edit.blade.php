@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Edit {{$kelas->namaKelas}}</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <p><a href="/kelas" class="btn btn-primary my-3 mx-4">Kembali</a></p>
    <div class="card-body">
      <form action="/kelas/{{$kelas->id_kelas}}" method="POST">
        @csrf
        @method('put')
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th><h4>Data</h4></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h6>Nama Kelas</h6></td>
            <td> <h6>Kelas : <input type="text" name="namaKelas" value="{{$kelas->namaKelas}}"> </h6></td>
          </tr>
          <tr>
            <td><h6>Nama Wali Kelas</h6></td>
            <td> 
                  <select name="guru_id" id="">
                    @foreach ($guru as $item)
                    @if ($item->nipGuru === $kelas->waliKelas)
                    <option name="guru_id" value="{{$item->nipGuru}}" selected>{{$item->namaGuru}}</option>   
                    @else
                    <option name="guru_id" value="{{$item->nipGuru}}">{{$item->namaGuru}}</option>
                    @endif
                    @endforeach
                  </select>
            </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>


@endsection