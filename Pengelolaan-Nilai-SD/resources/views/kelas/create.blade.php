@extends('layouts.master')
@section('content')
<div class="row column_title">
  <div class="col-md-12">
    <div class="page_title">
      <h1>Tambah Kelas</h1>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <h4><a href="/kelas" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
    <div class="card-body">
      <form action="/kelas" method="POST">
        @csrf
      <table id="tabel" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>
              <h4>Informasi</h4>
            </th>
            <th><h4>Data</h4></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h6>ID Kelas</h6></td>
            <td> <h6> <input type="number" name="idKelas"> </h6></td>
          </tr>
          <tr>
            <td><h6>Nama Kelas</h6></td>
            <td> <h6><input type="text" name="namaKelas"> </h6></td>
          </tr>
          <tr>
            <td><h6>Nama Wali Kelas</h6></td>
            <td> 
                  <select name="nipGuru" id="">
                    <option value="">--Pilih Wali Kelas--</option>
                    @foreach ($guru as $item)
                    <option name="nipGuru" value="{{$item->nipGuru}}">{{$item->namaGuru}}</option>
                    @endforeach
                  </select>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> # </th>
              <th><h5>Nama Mata Pelajaran</h5></th>
              <th><h5>Guru Pengampu</h5></th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($mapel as $key => $item)
            <tr>
              <td>{{$key + 1}}</td>
              <td><h6>{{$item->namaMapel}}</h6></td>
              <td>
                  <select name="guru_id[]" id="">
                  <option value="">--Pilih Guru Pengampu--</option>
                  @foreach ($guru as $value)
                      <option name="guru_id[]" value="{{$value->nipGuru}}" >{{$value->namaGuru}}</option>
                  @endforeach
                  </select>
              </td>
              <td><input type="checkbox" name="mapel_id[]" value="{{$item->id_mapel}}"></td>
            </tr>
            @empty
                <h4>Data Kosong</h4>
            @endforelse
              </tbody>
            </table>
      <button type="submit" class="btn btn-info float-right">Selesai</button>
    </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>


@endsection