@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <h4 class="card-title"><a href="/kelas/create" class="btn btn-primary my-3">Tambah Kelas</a> </h4>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
                <tr>
                  <th> <h5>Nama Kelas</h5> </th>
                  <th> <h5>Nama Wali Kelas</h5></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @forelse ($kelas as $key => $item)
                  <tr>
                    <td><h6>{{$item->namaKelas}}</h6></td>
                    <td><h6>{{$item->namaGuru}}</h6></td>   
                    <td>
                      <form action="/kelas/{{$item->id_kelas}}" method="POST" class="float-right">
                        @csrf
                        @method('delete')
                      <a href="/kelas/{{$item->id_kelas}}/ngajar" class="btn btn-info btn-sm">Detail</a>
                      <a href="/kelas/{{$item->id_kelas}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                    </form>
                    </td>
                  </tr> 
                @empty
                    <h4>Data Kosong</h4>
                @endforelse
              </tbody>
          </table>
          <script>
            $(document).ready(function() {
            $('#tabel').DataTable();
          } );
        </script>
        </div>
      </div>
    </div>
  </div>
@endsection
