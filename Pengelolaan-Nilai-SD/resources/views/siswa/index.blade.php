@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <h4 class="card-title"><a href="/siswa/create" class="btn btn-primary my-3">Tambah Siswa</a> </h4>
    <div class="card">
      <div class="card-body">
        @foreach ($kelas as $value)
        <div class="table-responsive">   
          <table class="table table-bordered">
            <h4 class="card-title">{{$value->namaKelas}}</h4> 
            <thead>
                <tr>
                  <th> <h5>Nama Lengkap</h5></th>
                  <th> <h5>Umur</h5></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @forelse ($siswa as $key => $item)
                @if ($value->id_kelas == $item->kelas_id)
                <tr>
                  <td><h6>{{$item->namaSiswa}}</h6></td>
                  <td><h6>{{$item->umurSiswa}}</h6></td>
                  <td>
                    <form action="/siswa/{{$item->id_siswa}}" method="POST" class="float-right">
                      @csrf
                      @method('delete')
                      <a href="/siswa/{{$item->id_siswa}}" class="btn btn-info">Detail</a>
                      <a href="/siswa/{{$item->id_siswa}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" class="btn btn-danger btn-sm" value="Hapus"> 
                    </form>
                  </td>
                </tr>                                        
              </tbody>                   
              @endif
              @empty
              <h4>Data Kosong</h4>   
             @endforelse  
          </table>
          <br><br>
          @endforeach
        <script>
            $(document).ready(function() {
            $('#tabel').DataTable();
          } );
        </script>
        </div>
      </div>
    </div>
  </div>
@endsection