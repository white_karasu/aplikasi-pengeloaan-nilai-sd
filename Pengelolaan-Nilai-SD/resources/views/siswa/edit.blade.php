@extends('layouts.master')
@section('content')
<div class="row column_title">
    <div class="col-md-12">
       <div class="page_title">
          <h1>Edit Biodata Siswa</h1>
       </div>
    </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <p><a href="/siswa" class="btn btn-primary my-3 mx-4">Kembali</a></p>
      <div class="card-body">
        <form action="/siswa/{{$siswa->id_siswa}}" method="post">
          @csrf
          @method('put')
            <table id="tabel" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> <h4>Informasi</h4> </th>
                  <th>  </th>
                  <th> <h4>Data</h4> </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> <h6>NISN</h6> </td>
                  <td> : </td>
                  <td> <input type="number" name="id_siswa" value="{{$siswa->id_siswa}}"></td>   
              </tr>
                <tr>
                  <td> <h6>Nama Lengkap</h6> </td>
                  <td> : </td>
                  <td> <input type="text" name="namaSiswa" value="{{$siswa->namaSiswa}}"> </td>   
                </tr>
                <tr>
                    <td> <h6>Umur</h6> </td>
                    <td> : </td>
                    <td> <input type="number" name="umurSiswa" value="{{$siswa->umurSiswa}}"></td>   
                </tr>
                <tr>
                  <td>
                    <h6>Jenis Kelamin</h6>
                  </td>
                  <td> : </td>
                  <td> 
                    <select name="jkSiswa" id=""> 
                      <option>--jenis kelamin--</option>
                      <option name="jkSiswa" value="laki-laki">laki - laki</option>
                      <option name="jkSiswa" value="perempuan">perempuan</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h6>Kelas</h6>
                  </td>
                  <td> : </td>
                  <td> 
                    <select name="kelas_id" id=""> 
                      @foreach ($kelas as $item)
                      @if ($item->id_kelas === $siswa->kelas_id)
                      <option name="kelas_id" value="{{$item->id_kelas}}" selected>{{$item->namaKelas}}</option>    
                      @else
                      <option name="kelas_id" value="{{$item->id_kelas}}">{{$item->namaKelas}}</option>
                      @endif 
                          
                      @endforeach
                    </select>
                  </td>
                </tr>
                
                {{-- <tr>
                    <td> <h6>Tempat Lahir</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                 <tr>
                    <td> <h6>Tanggal Lahir</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                 <tr>
                    <td> <h6>Agama</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ayah </h6> </td>
                  <td> : </td>
                  <td> <input type="text"> </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ibu</h6> </td>
                  <td> : </td>
                  <td> <input type="text"> </td>   
                </tr>
                <tr>
                  <td> <h6>Kelas</h6> </td>
                  <td> : </td>
                  <td> <select name="cars" id="cars">
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                  </select></td>   
                </tr> --}}
              </tbody> 
            </table>
            <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
        </form>
            <script>
              $(document).ready(function() {
              $('#data_users_reguler').DataTable();
          } );
          </script>
      </div>
    </div>
</div>
@endsection