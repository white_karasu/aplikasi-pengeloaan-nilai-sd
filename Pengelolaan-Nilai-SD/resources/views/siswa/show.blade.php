@extends('layouts.master')
@section('content')
<div class="row column_title">
    <div class="col-md-12">
       <div class="page_title">
          <h1>Biodata Lengkap Siswa</h1>
       </div>
    </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
            <table id="tabel" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> <h4>Informasi</h4> </th>
                  <th>  </th>
                  <th> <h4>Data</h4> </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> <h6>NISN</h6> </td>
                  <td> : </td>
                  <td> <h6>{{$siswa->id_siswa}}</h6> </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Lengkap</h6> </td>
                  <td> : </td>
                  <td> <h6>{{$siswa->namaSiswa}}</h6> </td>   
                </tr>
                <tr>
                    <td> <h6>Umur</h6> </td>
                    <td> : </td>
                    <td> <h6>{{$siswa->umurSiswa}}</h6> </td>   
                </tr>
                <tr>
                  <td> <h6>Jenis Kelamin</h6> </td>
                  <td> : </td>
                  <td> <h6>{{$siswa->jkSiswa}}</h6> </td>   
                </tr>
                {{-- <tr>
                    <td> <h6>Tempat Lahir</h6> </td>
                    <td> : </td>
                    <td> <h6>Cianjur</h6> </td>   
                </tr>
                <tr>
                    <td> <h6>Tanggal Lahir</h6> </td>
                    <td> : </td>
                    <td> <h6>09 March 180 SM</h6> </td>   
                </tr>
                <tr>
                    <td> <h6>Agama</h6> </td>
                    <td> : </td>
                    <td> <h6>Islam</h6> </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ayah </h6> </td>
                  <td> : </td>
                  <td> <h6>Joko Widodo</h6> </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ibu</h6> </td>
                  <td> : </td>
                  <td> <h6>Megawati</h6> </td>   
                </tr> --}}
              </tbody>
            </table>
      <h4><a href="/siswa" class="btn btn-primary my-3 float-left">Kembali</a> </h4>
            <script>
              $(document).ready(function() {
              $('#data_users_reguler').DataTable();
          } );
          </script>
      </div>
    </div>
</div>
@endsection