@extends('layouts.master')
@section('content')
<div class="row column_title">
    <div class="col-md-12">
       <div class="page_title">
          <h1>Tambah Siswa</h1>
       </div>
    </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <h4><a href="/siswa" class="btn btn-primary mx-4 my-3">Kembali</a> </h4>
      <div class="card-body">
        <form action="/siswa" method="post">
          @csrf
            <table id="tabel" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th> <h4>Informasi</h4> </th>
                  <th>  </th>
                  <th> <h4>Data</h4> </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> <h6>NISN</h6> </td>
                  <td> : </td>
                  <td> <input type="number" name="id_siswa"></td>   
              </tr>
                <tr>
                  <td> <h6>Nama Lengkap</h6> </td>
                  <td> : </td>
                  <td> <input type="text" name="namaSiswa"> </td>   
                </tr>
                <tr>
                    <td> <h6>Umur</h6> </td>
                    <td> : </td>
                    <td> <input type="number" name="umurSiswa"></td>   
                </tr>
                <tr>
                  <td>
                    <h6>Jenis Kelamin</h6>
                  </td>
                  <td> : </td>
                  <td> 
                    <select name="jkSiswa" id=""> 
                      <option value="">--Pilih Jenis Kelamin--</option>
                      <option name="jkSiswa" value="laki-laki">laki - laki</option>
                      <option name="jkSiswa" value="perempuan">perempuan</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h6>Kelas</h6>
                  </td>
                  <td> : </td>
                  <td> 
                    <select name="kelas_id" id="">
                      <option value="">--Pilih Kelas--</option> 
                      @foreach ($kelas as $item)
                      <option name="kelas_id" value="{{$item->id_kelas}}">{{$item->namaKelas}}</option>    
                      @endforeach
                    </select>
                  </td>
                </tr>
                
                {{-- <tr>
                    <td> <h6>Tempat Lahir</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                 <tr>
                    <td> <h6>Tanggal Lahir</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                 <tr>
                    <td> <h6>Agama</h6> </td>
                    <td> : </td>
                    <td> <input type="text" > </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ayah </h6> </td>
                  <td> : </td>
                  <td> <input type="text"> </td>   
                </tr>
                <tr>
                  <td> <h6>Nama Ibu</h6> </td>
                  <td> : </td>
                  <td> <input type="text"> </td>   
                </tr>
                <tr>
                  <td> <h6>Kelas</h6> </td>
                  <td> : </td>
                  <td> <select name="cars" id="cars">
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                  </select></td>   
                </tr> --}}
              </tbody> 
            </table>
            <button type="submit" class="btn btn-primary float-right">Selesai</button>
        </form>
            <script>
              $(document).ready(function() {
              $('#data_users_reguler').DataTable();
          } );
          </script>
      </div>
    </div>
</div>
@endsection