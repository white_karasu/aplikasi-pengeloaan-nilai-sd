@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <div class="row column_title">
    <div class="col-md-12">
      <div class="page_title">
        <h1>Detail Sekolah</h1>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <form action="/sekolah/{{$sekolah->npsn}}" method="POST">
        @csrf
      @method('put')
      <table id="tabel" class="table table-bordered table-striped text-black">
        <thead>
          <tr>
            <th>
              <h3>Informasi</h3>
            </th>
            <th> </th>
            <th>
              <h3>Data</h3>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Dinas Pendidikan & Kebudayaan</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->dispenBud}} </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Sekolah</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->namaSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Nama Kepala Sekolah</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="kepalaSekolah" required value="{{$sekolah->kepalaSekolah}}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Tahun Pelajaran</h6>
            </td>
            <td> : </td>
            <td> <input type="text" name="tahunPelajaran" required value="{{$sekolah->tahunPelajaran}}"> </td>
          </tr>
          <tr>
            <td>
              <h6>Semester</h6>
            </td>
            <td> : </td>
            <td> <select name="semesterSekolah" required value="{{$sekolah->semesterSekolah}}">
                <option value="ganjil">1 (Ganjil)</option>
                <option value="genap">2 (Genap)</option>
              </select> </td>
          </tr>
          <tr>
            <td>
              <h6>NSS</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->nssSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Alamat Sekolah</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->alamatSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Kelurahan / Desa</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->desaSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Kecamatan</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->kecamatanSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Kota / Kabupaten</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->kotaSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>Provinsi</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->provinsiSekolah}} </td>
          </tr>
          <tr>
            <td>
              <h6>NPSN</h6>
            </td>
            <td> : </td>
            <td> {{$sekolah->npsn}} </td>
          </tr>
        </tbody>
      </table>
      <button type="submit" class="btn btn-warning my-3 float-right">Simpan</button>
      <a href="/sekolah" class="btn btn-primary my-3 float-left">Kembali</a>
      </form>
      <script>
        $(document).ready(function() {
          $('#data_users_reguler').DataTable();
        });
      </script>
    </div>
  </div>
</div>
@endsection