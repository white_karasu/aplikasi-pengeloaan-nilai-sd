@extends('layouts.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h2 class="card-title">Data Sekolah</h2> &nbsp;
      <div class="table-responsive">
        <table id="tabel" class="table table-bordered table-striped" style="width:100%">
          <thead>
            <tr>
              <th> # </th>
              <th>
                <h5>Nama Sekolah</h5>
              </th>
              <th>
                <h5>Nama Kepala Sekolah</h5>
              </th>
              <th>
                <h5>Tahun Pelajaran</h5>
              </th>
              <th>
                <h5>Semester</h5>
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>
                <h6>{{$sekolah->namaSekolah}}</h6>
              </td>
              <td>
                <h6>{{$sekolah->kepalaSekolah}}</h6>
              </td>
              <td>
                <h6>{{$sekolah->tahunPelajaran}}</h6>
              </td>
              <td>
                <h6>{{$sekolah->semesterSekolah}}</h6>
              </td>
              <td>
                <a href="/sekolah/{{$sekolah->npsn}}/edit" class="btn btn-warning btn-sm">Edit</a>
              </td>
            </tr>
          </tbody>
        </table>
        <script>
          $(document).ready(function() {
            $('#data_users_reguler').DataTable();
          });
        </script>
      </div>
    </div>
  </div>
</div>
@endsection