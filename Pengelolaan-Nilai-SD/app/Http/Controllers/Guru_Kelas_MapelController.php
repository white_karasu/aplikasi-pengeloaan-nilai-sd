<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Ngajar;
use App\Models\Kelas;
use App\Models\Mapel;

class Guru_Kelas_MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $siswa = DB::table('siswa')
        ->where('kelas_id', $id)
        ->get();
        $kelas = Kelas::find($id);
        $ngajar = Ngajar::where('kelas_id', $id)->get();
        return view('ngajar.index', compact('siswa', 'kelas', 'ngajar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $guru = DB::table('guru')->get();
        $kelas = DB::table('kelas')
        ->where('id_kelas', $id)
        ->first();
        $data = DB::table('mapel')
                ->get();
                
        return view('ngajar.create', compact('guru','data', 'kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\joinHttp\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $guru_id = $request->guru_id;
        $mapel_id = $request->mapel_id;
        for($i= 0 ; $i<count($mapel_id);$i++){
        $datasave = [
            'kelas_id' => $request['kelas_id'],
            'mapel_id' => $mapel_id[$i],
            'guru_id' => $guru_id[$i],
        ];
        DB::table('ngajar')->updateOrInsert($datasave);
        }
        return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = DB::table('guru')->get();
        $mapel = DB::table('mapel')->get();
        $kelas = DB::table('kelas')
        ->first();
        $ngajar = DB::table('ngajar')
        ->where('id_ngajar', '=', $id)
        ->first();
        return view('ngajar.edit', compact('guru', 'mapel','kelas', 'ngajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('ngajar')
            ->where('id_ngajar', $id)
            ->update
                ([
                    'mapel_id' => $request['mapel_id'],
                    'guru_id' => $request['guru_id'],
                ]);
        $kelas = DB::table('kelas')->first();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('ngajar')
        ->where('id_ngajar', $id)
        ->delete();
        return redirect()->back();
    }
}
