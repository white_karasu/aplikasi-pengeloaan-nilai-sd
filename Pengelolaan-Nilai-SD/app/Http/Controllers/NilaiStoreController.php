<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class NilaiStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $tugas1 = $request->ts1;
                $tugas2 = $request->ts2;
                $uts = $request->uts;
                $tugas3 = $request->ts3;
                $tugas4 = $request->ts4;
                $uas = $request->uas;
                $na = $request->na;
                $siswa = $request->siswa_id;
                
                for($i= 0 ; $i<count($siswa);$i++){
                    
                $datasave = [
                    'siswa_id' => $siswa[$i],
                    'kelas_id' => $request['kelas_id'],
                    'mapel_id' => $request['mapel_id'],
                    'tS1' => $tugas1[$i],
                    'tS2' => $tugas2[$i],
                    'uts' => $uts[$i],
                    'tS3' => $tugas3[$i],
                    'tS4' => $tugas4[$i],
                    'uas' => $uas[$i],
                    'nilaiAkhir' => $na[$i],
                ];
                DB::table('nilai')->update($datasave);
                }
                return redirect ('/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('rapot.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        
        return view('rapot.show');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id_mapel)
    {
        $siswa = DB::table('nilai')
        ->where('siswa_id', $id_mapel)
        ->first();
        $tugas1 = $request->ts1;
                $tugas2 = $request->ts2;
                $uts = $request->uts;
                $tugas3 = $request->ts3;
                $tugas4 = $request->ts4;
                $uas = $request->uas;
                $na = $request->na;
                $siswa = $request->siswa_id;
                
                for($i= 0 ; $i<count($siswa);$i++){
                    
                $datasave = [
                    'siswa_id' => $siswa[$i],
                    'kelas_id' => $request['kelas_id'],
                    'mapel_id' => $request['mapel_id'],
                    'tS1' => $tugas1[$i],
                    'tS2' => $tugas2[$i],
                    'uts' => $uts[$i],
                    'tS3' => $tugas3[$i],
                    'tS4' => $tugas4[$i],
                    'uas' => $uas[$i],
                    'nilaiAkhir' => $na[$i],
                ];
                DB::table('nilai')->update($datasave);
                }
                return redirect ('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
