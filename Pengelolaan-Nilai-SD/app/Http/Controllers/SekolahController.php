<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function index()
    {
        $sekolah = DB::table('sekolah')
        ->first();
        return view('sekolah.index', ['sekolah' => $sekolah]);
    }
    public function edit($npsn)
    {
        $sekolah = DB::table('sekolah')
        ->where('npsn', $npsn)
        ->first();
        return view('sekolah.edit', compact('sekolah'));
    }
    public function update($npsn, Request $request)
    {
        DB::table('sekolah')
            ->where('npsn', $npsn)
            ->update
                ([
                    'kepalaSekolah' => $request['kepalaSekolah'],
                    'tahunPelajaran' => $request['tahunPelajaran'],
                    'semesterSekolah' => $request['semesterSekolah']
                ]);
        return redirect('/sekolah');
    }
    
}