<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $kelas = DB::table('kelas')
                ->join('guru', 'nipGuru', '=', 'kelas.waliKelas')
                ->select('guru.namaGuru','kelas.*')
                ->orderBy('namaKelas', 'asc')
                ->get();
            return view('kelas.index', compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guru = DB::table('guru')->get();
        $mapel = DB::table('mapel')->get();
        $kelas = DB::table('kelas')->get();
        $ngajar = DB::table('ngajar')->get();
        return view('kelas.create', compact('guru', 'mapel','kelas', 'ngajar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('kelas')->insert
                ([   
                    'id_kelas' => $request['idKelas'],
                    'namaKelas' => $request['namaKelas'],
                    'waliKelas' => $request['nipGuru'],
                ]);

                $guru_id = $request->guru_id;
                $mapel_id = $request->mapel_id;
                for($i= 0 ; $i<count($mapel_id);$i++){
                    
                $datasave = [
                    'kelas_id' => $request['idKelas'],
                    'guru_id' => $guru_id[$i],
                    'mapel_id' => $mapel_id[$i],
                ];
                DB::table('ngajar')->updateOrInsert($datasave);
                }
                return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelas = DB::table('kelas')
        ->where('id_kelas', $id)
        ->first();
        $siswa = DB::table('siswa')
        ->where('kelas_id', $id)
        ->get();
        return view('kelas.show', compact('kelas','siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = DB::table('kelas')->first();
        $guru = DB::table('guru')->get();
        return view('kelas.edit', compact('kelas', 'guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('kelas')
            ->where('id_kelas', $id)
            ->update
                ([
                    'namaKelas' => $request['namaKelas'],
                    'waliKelas' => $request['guru_id'],
                ]);
        return redirect('/kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kelas')
        ->where('id_kelas', $id)
        ->delete();
        return redirect('/kelas');
    }
}
