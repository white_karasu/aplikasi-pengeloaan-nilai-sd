<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = DB::table('siswa')
        ->orderBy('namaSiswa', 'asc')
        ->get();
        $kelas = DB::table('kelas')
        ->orderBy('namaKelas', 'asc')
        ->get();
        $hitung = DB::table('siswa')->count();
        return view('siswa.index', compact('hitung','siswa', 'kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = DB::table('kelas')
        ->orderBy('namaKelas', 'asc')->get();
        return view('siswa.create', compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('siswa')->insert
        ([   
            'id_siswa' => $request['id_siswa'],
            'namaSiswa' => $request['namaSiswa'],
            'umurSiswa' => $request['umurSiswa'],
            'jkSiswa' => $request['jkSiswa'],
            'kelas_id' => $request['kelas_id'],
        ]);
        DB::table('nilai')->insert
        ([
            'siswa_id' => $request['id_siswa'],
            'kelas_id' => $request['kelas_id'],
        ]);

        return redirect('/siswa');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = DB::table('siswa')
        ->where('id_siswa', $id)
        ->first();
        return view('siswa.show', compact('siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = DB::table('siswa')
        ->where('id_siswa', $id)
        ->first();
        $kelas = DB::table('kelas')->get();
        return view('siswa.edit', compact('siswa', 'kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('siswa')
        ->where('id_siswa', $id)
            ->update
        ([   
            'id_siswa' => $request['id_siswa'],
            'namaSiswa' => $request['namaSiswa'],
            'umurSiswa' => $request['umurSiswa'],
            'jkSiswa' => $request['jkSiswa'],
            'kelas_id' => $request['kelas_id'],
        ]);
    return redirect('/siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('siswa')
        ->where('id_siswa', $id)
        ->delete();
        return redirect('/siswa');
    }
}
