<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Mapel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Ngajar;
use App\Models\Nilai;

class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = DB::table('kelas')
        ->orderBy('namaKelas', 'asc')
        ->get();
        return view('nilai.index', compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mapel($id)
    {
        $kelas = DB::table('kelas')
        ->where('id_kelas', $id)
        ->first();
        $mapel = Ngajar::where('kelas_id', $id)->get();
        return view('nilai.mapel', compact('kelas', 'mapel'));
    }
    public function input($is, Request $j)
    {
        $kelas = Kelas::where('id_kelas', $is)->first();
        $ngajar = Ngajar::where('mapel_id', 1)->first();
        $siswa = Nilai::where('kelas_id', $is)->get();
        return view('nilai.input', compact('siswa','kelas', 'ngajar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ngajar = Ngajar::where('kelas_id', $id)->get();
        $kelas = DB::table('kelas')
        ->where('id_kelas', $id)
        ->first();
        return view('nilai.show', compact('ngajar','kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
