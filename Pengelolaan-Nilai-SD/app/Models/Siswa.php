<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table = 'siswa';
    public $timestamps = false;
    protected $primaryKey = 'id_siswa';

    public function kelas()  
    {
        return $this->belongsTo(Kelas::class, 'kelas_id', 'id_kelas');
    }
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'siswa_id', 'id_siswa');
    }
    public function mapel()
    {
        return $this->belongsToMany(Mapel::class,'nilai','kelas_id', 'siswa_id', 'mapel_id');
    }
}
