<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    use HasFactory;
    protected $table = 'nilai';
    
    public $timestamps = false;
    protected $fillable = ['siswa_id', 'kelas_id', 'mapel_id', 'mapel_id', 
                            'tS1', 'tS2', 
                            'uts', 
                            'tS3', 'tS4',	
                            'uas',	
                            'nilaiAkhir'	];
    public function kelas()  
    {
        return $this->belongsTo(Kelas::class, 'kelas_id', 'id_kelas');
    }
    public function mapel()  
    {
        return $this->belongsTo(Mapel::class, 'mapel_id', 'id_mapel');
    }
    public function siswa()  
    {
        return $this->belongsTo(Siswa::class, 'siswa_id', 'id_siswa');
    }
}
