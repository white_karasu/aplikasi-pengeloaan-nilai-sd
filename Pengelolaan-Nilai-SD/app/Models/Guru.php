<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;
    protected $table = 'guru';
    public $timestamps = false;
    protected $primaryKey = 'nipGuru';

    public function kelas()
    {
        return $this->belongsToMany(Kelas::class,'ngajar', 'kelas_id', 'guru_id', 'mapel_id');
    }
    public function mapel()
    {
        return $this->belongsToMany(Mapel::class,'ngajar','kelas_id', 'guru_id', 'mapel_id');
    }
    public function ngajar()
    {
        return $this->hasMany(Ngajar::class, 'guru_id', 'nipGuru');
    }
}
