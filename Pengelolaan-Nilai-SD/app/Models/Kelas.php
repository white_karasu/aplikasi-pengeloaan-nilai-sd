<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;
    protected $table = 'kelas';
    
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id_kelas';

    public function guru()
    {
        return $this->belongsToMany(Guru::class,'ngajar','kelas_id', 'guru_id', 'mapel_id');
    }
    public function mapel()
    {
        return $this->belongsToMany(Mapel::class,'ngajar','kelas_id', 'guru_id', 'mapel_id');
    }
    public function ngajar()
    {
        return $this->hasMany(Ngajar::class, 'kelas_id', 'id_kelas');
    }
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'kelas_id', 'id_kelas');
    }
}
