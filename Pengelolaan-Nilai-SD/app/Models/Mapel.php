<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    use HasFactory;
    protected $table = 'mapel';
    public $timestamps = false;
    protected $primaryKey = 'id_mapel';

    public function kelas()
    {
        return $this->belongsToMany(Kelas::class,'ngajar','kelas_id', 'guru_id', 'mapel_id');
    }
    public function guru()
    {
        return $this->belongsToMany(Guru::class,'ngajar','kelas_id', 'guru_id', 'mapel_id');
    }
    public function ngajar()
    {
        return $this->hasMany(Ngajar::class, 'mapel_id', 'id_mapel');
    }
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'mapel_id', 'id_mapel');
    }
}
