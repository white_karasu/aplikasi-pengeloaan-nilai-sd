<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ngajar extends Model
{
    use HasFactory;
    protected $table = 'ngajar';
    protected $primaryKey = 'id_ngajar';
    
    public $timestamps = false;
    protected $fillable = ['guru_id', 'kelas_id', 'mapel_id'];

    public function guru()  
    {
        return $this->belongsTo(Guru::class, 'guru_id', 'nipGuru');
    }
    public function kelas()  
    {
        return $this->belongsTo(Kelas::class, 'kelas_id', 'id_kelas');
    }
    public function mapel()  
    {
        return $this->belongsTo(Mapel::class, 'mapel_id', 'id_mapel');
    }

    
    
}
